<table><caption>thissux</caption>
<tr><th scope="col">Geography</th><th scope="col">Q1</th><th scope="col">Q2</th><th scope="col">Q3</th><th scope="col">Q4</th></tr>
<tr><td>Canada</td><td>35,335,266</td><td>35,416,179</td><td>35,540,419</td><td>35,675,834</td></tr>
<tr><td>Newfoundland and Labrador</td><td>528,479</td><td>527,094</td><td>526,977</td><td>526,837</td></tr>
<tr><td>Prince Edward Island</td><td>145,482</td><td>145,789</td><td>146,283</td><td>146,524</td></tr>
<tr><td>Nova Scotia</td><td>943,063</td><td>942,190</td><td>942,668</td><td>943,932</td></tr>
<tr><td>New Brunswick</td><td>754,986</td><td>754,176</td><td>753,914</td><td>754,643</td></tr>
<tr><td>Quebec</td><td>8,179,010</td><td>8,190,604</td><td>8,214,672</td><td>8,236,310</td></tr>
<tr><td>Ontario</td><td>13,615,388</td><td>13,640,187</td><td>13,678,740</td><td>13,730,187</td></tr>
<tr><td>Manitoba</td><td>1,273,158</td><td>1,276,545</td><td>1,282,043</td><td>1,286,323</td></tr>
<tr><td>Saskatchewan</td><td>1,115,244</td><td>1,120,129</td><td>1,125,410</td><td>1,129,899</td></tr>
<tr><td>Alberta</td><td>4,059,691</td><td>4,086,639</td><td>4,121,692</td><td>4,145,992</td></tr>
<tr><td>British Columbia</td><td>4,604,720</td><td>4,616,626</td><td>4,631,302</td><td>4,657,947</td></tr>
<tr><td>Yukon</td><td>36,167</td><td>36,225</td><td>36,510</td><td>36,758</td></tr>
<tr><td>Northwest Territories</td><td>43,933</td><td>43,725</td><td>43,623</td><td>43,795</td></tr>
<tr><td>Nunavut</td><td>35,945</td><td>36,250</td><td>36,585</td><td>36,687</td></tr></table>