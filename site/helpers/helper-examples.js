
function readData(aFileName) {
	var fs = require('fs');
	var lOut;
	lOut = fs.readFileSync(aFileName, {"encoding":"utf8"})

	return lOut;
}

function doExample(qRegion, qTitle, qType, qSynopsys, qDetails) {
	var lOut ='';	
	var lFiles = qDetails.split(",");
	var lSynopsys = require("../data/Examples/" + qSynopsys + ".json");

	//lOut = '<h2 class="mrgn-tp-0 h3">' + qRegion + '</h2>';
	//lOut += '<h3>' + qTitle + '</h3>';

	for (lFile in lFiles) {		
		var lContents = readData("site/data/Examples/" + lFiles[lFile] + ".html");		
		lOut += '<div class="row"><div class="col-md-12"><h2>' + lSynopsys[lFile] + '</h2>' + lContents + '</div></div>';		
	}
	
	return lOut;
}

module.exports.register = function (Handlebars, options, params)  {
	Handlebars.registerHelper('example', function (qRegion, qTitle, qType, qSynopsys, qDetails)  {
		return doExample(qRegion, qTitle, qType, qSynopsys, qDetails);
	});
};