function filterCode(aIn) {
	var lOut = aIn;

	lOut = lOut.replace(/</g, "&lt;");
	lOut = lOut.replace(/>/g, "&gt;");
	lOut = lOut.replace(/"/g, "&quot;");
	//console.log (lOut);
	return lOut;
}

function doTryItSolution(qFile, qChoice) {
	var fs = require('fs');	
	var lInclude;
	var lOut = '';
	var lJSData = fs.readFileSync("site/data/Scenarios/" + qFile +".json", {"encoding":"utf8"});
	var lData = JSON.parse(lJSData);	

	/*parse the json file and build the results block */
	for (key in lData) {
		if (lData.hasOwnProperty(key)) {
			if (key != "css" && key != "src" ) {
				/* new section */
				if (lData[key]["l"]["choice"] == qChoice) {
					lInclude = doTryItInclude(qFile + "/" + lData[key]["l"]["file"]);
				} else {
					lInclude = doTryItInclude(qFile + "/" + lData[key]["r"]["file"]);
				}
				
				lOut = lOut + lInclude;
			}
		}
	}

	return lOut;
}

function doTryItInclude(qFile, qFiletered) {
	var fs = require('fs');
	var lOut;
	lOut = fs.readFileSync("site/data/Scenarios/" + qFile +".txt", {"encoding":"utf8"})

	if (qFiletered != undefined) {
		return filterCode(lOut);
	} else {
		return lOut;
	}
}

function doTryItPartsLine(aData, aKey, aSide, qFile) {
	var lOut = '';
	var lInclude = filterCode(doTryItInclude(qFile + "/" + aData[aKey][aSide]["file"]));
	lOut = lOut + "<div id=\"row" + aKey + "col" + aSide + "\" class=\"col-md-6\">";
	lOut = lOut + "<div class=\"row\">";
	lOut = lOut + "<input name=\"row" + aKey + "\" id=\"row" + aKey + aSide + "\" type=\"radio\">";
	lOut = lOut + "<label for=\"row" + aKey + aSide + "\">" + aData[aKey][aSide]["title"] + "</label></div>" ;
	lOut = lOut + "<div class=\"row\">";
	lOut = lOut + "<input id=\"row" + aKey + aSide + "hidden\" type=\"hidden\" value=\"" + lInclude + "\">";
	lOut = lOut + "<details><summary>Open to reveal the code</summary>";
	lOut = lOut + "<code class=\"prettyprint\">" + lInclude + "</code>";
	lOut = lOut + "</details></div></div>\n";
/*	lOut = lOut + doTryItInclude(qFile + "/" + aData[aKey][aSide]["file"])  + "</div>\n"*/

	return lOut;
};

function doTryItCSS(qFile) {
	var fs = require('fs');
	var lOut = '';
	var lJSData = fs.readFileSync("site/data/Scenarios/" + qFile +".json", {"encoding":"utf8"});
	var lData =   JSON.parse(lJSData);	

	/*parse the json file and build the options block */
	for (key in lData) {
		if (lData.hasOwnProperty(key)) {
			if (key == "css") {
				/* new section */
				lOut = "<link rel=\"stylesheet\" href=\"css/tryit/" + lData[key] + "\" />";
			}
		}
	}

	return lOut;
}

function doTryItParts(qFile) {
	var fs = require('fs');
	var lOut = '';
	var lJSData = fs.readFileSync("site/data/Scenarios/" + qFile +".json", {"encoding":"utf8"});
	var lData =   JSON.parse(lJSData);	
	var lSRC = '';

	/*parse the json file and build the options block */
	for (key in lData) {
		if (lData.hasOwnProperty(key)) {
			if (key != "css" && key != "src" ) {
				/* new section */
				lOut = lOut + "<fieldset><legend>Choose " + lData[key]["title"] + "</legend>";
				lOut = lOut + doTryItPartsLine(lData, key, "l", qFile);
				lOut = lOut + doTryItPartsLine(lData, key, "r", qFile);			
				lOut = lOut + "</fieldset>";
			} else if (key == "src") {
				lSRC = "<div class=\"row\"><div class=\"col-md-12 small\">Source: " + lData[key] + "</div></div>"
			}
		}
	}

	return lOut + lSRC;
}

module.exports.register = function (Handlebars, options, params)  {
	Handlebars.registerHelper('tryit-include', function (qFile, qFiletered)  {
		return doTryItInclude(qFile, qFiletered);
	});

	Handlebars.registerHelper('tryit-css', function (qFile)  {
		return doTryItCSS(qFile);
	});

	Handlebars.registerHelper('tryit-parts', function (qFile)  {
		return doTryItParts(qFile);
	});

	Handlebars.registerHelper('tryit-solutions', function (qFile, qChoice)  {
		return doTryItSolution(qFile, qChoice);
	});
};